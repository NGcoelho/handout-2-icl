compile:
	java -cp ./out/production/ParserWTypeChecker/ console.CompilerShell

test:
	java -jar junit-platform-console-standalone-1.3.2.jar -cp ./out/test/ParserWTypeChecker/ -c CompilerTests -cp ./out/production/ParserWTypeChecker/
all:
	java -jar jasmin.jar *.j
run:
	java Demo

clean:
	rm *.j *.class

