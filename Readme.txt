Para testar o projeto foi disponibilizado um Makefile, que dispoe de 4
comandos:
	- make compile : inicia o compiler para introduzir expressoes

	- make clean: remove todos os ficheiros .j e .class
	
	- make all : compila source code Jasmin (.j)

	- make run : corre o ficheiro bytecode Jasmin (.class)

Adicionalmente, foi ainda introduzido o comando make test : executa os 4 
passos acima referidos autonomamente para um conjunto de testes jUnit.
	
Trabalho realizado por

Nuno Coelho 42844
Ricardo Loureiro 45351
João Pedro Afonso 42960
