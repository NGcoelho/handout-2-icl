package ast;

import compUtils.CodeBlock;
import types.IType;
import types.refType;
import values.*;
import environment.*;

public class ASTAssign implements ASTNode {

	private ASTNode left, right;
	private IType type;
	
	public ASTAssign(ASTNode t1, ASTNode t2){
		left=t1;
		right=t2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException,BadTypeException {
		
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		if(l instanceof ReferenceValue) {
			((ReferenceValue)l).setValue(r);
			return r;		
		}
		throw new BadTypeException("Invalid type, expected reference");
			
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {

		IType left = this.left.typecheck(env);
		IType right = this.right.typecheck(env);

		if(left instanceof refType){
			refType ref_left = (refType)left;
			if(ref_left.type.getInstance() == right.getInstance()){
				type = right;
				return right;
			}
		}
		throw new BadTypeException("Unexpected Reference!");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		block.emit_empty_line();
		block.emit_comment("Assignment");
		left.compile(block,env);
		block.emit_dup();
		right.compile(block, env);
		block.emit_put_idval(left.getType(),this.type);
		block.emit_assignReturnValue(left.getType(),this.type);
	}

	@Override
	public String toString(){
		return "("+left.toString()+":="+right.toString()+")";
	}

}
