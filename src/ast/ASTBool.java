package ast;

import compUtils.CodeBlock;
import types.IType;
import types.boolType;
import values.*;
import environment.*;

public class ASTBool implements ASTNode {
	boolean value;
	IType  type;

	public ASTBool(boolean b){ value=b; }

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException,DuplicateIDException,BadTypeException{
		return new boolValue(value);		
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException {
		type = boolType.instance;
		return boolType.instance;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException {
		block.emit_push(value ? 1 : 0);
	}

	@Override
	public String toString(){
		return Boolean.toString(value);	
	}	
}
