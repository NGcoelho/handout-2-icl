package ast;

import java.util.List;

import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.funType;
import values.*;

public class ASTCall implements ASTNode{
	ASTNode left;
	List<ASTNode> arguments;
	IType type;
	
	public ASTCall(ASTNode l, List<ASTNode> arguments) {
		left=l;
		this.arguments=arguments;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException,BadTypeException{
		Closure c = (Closure) left.eval(env);
		Environment<IValue> newEnv ;// c.getEnv();
		//newEnv=newEnv.beginScope();
		newEnv=env.beginScope();
		List<String> identifiers = c.getParameters();
		for(int i=0;i<arguments.size(); i++) {
			String id = identifiers.get(i);
			ASTNode node = arguments.get(i);
			newEnv.associate(id,node.eval(env));
		}

		return c.getBody().eval(newEnv);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {

		IType l = left.typecheck(env);
		if(l instanceof funType){
			funType fun = (funType) l;
			List<IType> types = fun.getParamTypes();
			if(types.size() == arguments.size()){
				for(int i = 0; i < types.size(); i++){
					IType arg = arguments.get(i).typecheck(env);
					IType expect = types.get(i);
					if(arg instanceof  funType && expect instanceof  funType && !arg.equals(expect))
						throw new BadTypeException("Wrong argument type in function");

					else if(arg.getInstance() != expect.getInstance())
						throw new BadTypeException("Arg: "+ arg.toString() + "; Expected : "+ expect.toString());
				}
			}
			else throw new UndeclaredIDException("Undeclared arguments");
			this.type = fun.getReturnType();
			return type;
		}
		throw  new BadTypeException("Expected function!");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		block.emit_empty_line();
		block.emit_comment("AST Call");
		funType function = (funType)left.getType();
		left.compile(block, env);
		block.emit_checkcast(function);
		for(ASTNode a : arguments)
			a.compile(block, env);
		block.emit_function_call(function,arguments.size());
	}

	@Override
	public String toString(){
		return left.toString()+" ("+arguments.toString()+")";
	}
}
