package ast;

import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.refType;
import values.*;

public class ASTDeref implements ASTNode{
	ASTNode value;
	IType type;
	
	public ASTDeref(ASTNode value){
		this.value=value;	
	}

	@Override
	public String toString(){
		return value.toString();	
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue v = value.eval(env);
		if( v instanceof ReferenceValue )
			return ((ReferenceValue)v).getValue();
		throw new BadTypeException("ERROR: reference expected");	
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType type = value.typecheck(env);
		if(type instanceof refType){
			this.type = type;
			return ((refType)type).type;
		}
		throw new BadTypeException("Expected reference..");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		value.compile(block,env);
		block.emit_get_value(getType());
	}


}
