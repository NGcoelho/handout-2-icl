package ast;

import compUtils.CodeBlock;
import types.IType;
import types.boolType;
import values.*;
import environment.*;

public class ASTDisj implements ASTNode{
 	ASTNode left,right;
 	private IType type;
	
	public ASTDisj(ASTNode t1,ASTNode t2){
		left=t1;
		right=t2;	
	}

	@Override
	public String toString(){
		return "("+left.toString()+" || "+right.toString()+")";	
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException,BadTypeException{
		IValue l = left.eval(env);
		IValue r = right.eval(env);

		if(l instanceof boolValue && r instanceof boolValue)
			return new boolValue(((boolValue)l).getValue() || ((boolValue)r).getValue());
		throw new BadTypeException("Invalid type, expected boolean");
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType left = this.left.typecheck(env);
		IType right = this.right.typecheck(env);
		if(left instanceof boolType && right instanceof  boolType){
			type = left;
			return left;
		}
		throw new BadTypeException("Expected boolean");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		left.compile(block,env);
		right.compile(block,env);
		block.emit_or();
	}
}
