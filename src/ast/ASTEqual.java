package ast;
import compUtils.CodeBlock;
import types.IType;
import types.boolType;
import types.intType;
import values.IValue;
import values.*;
import environment.*;

public class ASTEqual implements ASTNode{

	private ASTNode left, right;
	private  IType type;
	public ASTEqual(ASTNode t1, ASTNode t2){
		left=t1;
		right=t2;
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue l=left.eval(env);
		IValue r=right.eval(env);
		if(l instanceof boolValue && r instanceof boolValue)
			return new boolValue(((boolValue)l).getValue() == ((boolValue)r).getValue());
		else if(l instanceof intValue && r instanceof intValue)
			return new boolValue(((intValue)l).getValue() == ((intValue)r).getValue());
		else if(l instanceof ReferenceValue && r instanceof ReferenceValue)
			return new boolValue(((ReferenceValue)l).getValue() == ((ReferenceValue)r).getValue());

		 else throw new BadTypeException("Syntax Error");
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType left = this.left.typecheck(env);
		IType right =this.right.typecheck(env);
		if((left instanceof boolType && right instanceof boolType) || (left instanceof intType && right instanceof intType)){
			type = boolType.instance;
			return boolType.instance;
		}
		throw new BadTypeException("Expected same type!");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		left.compile(block,env);
		right.compile(block, env);
		block.emit_eq();
	}

	@Override
	public String toString() {
		return "("+left.toString()+"=="+right.toString()+")";
	}

	
}

