package ast;

import compUtils.CodeBlock;
import types.IType;
import types.objType;
import types.recType;
import types.refType;
import values.*;
import environment.*;

public class ASTFieldAccess implements ASTNode{
	private ASTNode left;
	private String id;
	private IType type;
	
	public ASTFieldAccess(ASTNode left,String id){
		this.left = left;
		this.id = id;
	}
		
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue value = left.eval(env);
		if(value instanceof RecordValue)
			return ((RecordValue)value).getFromLabel(id);
		else if(value instanceof ObjectValue){
			IValue field = ((ObjectValue)value).getFromLabel(id);
			return field;		
		}
		throw new BadTypeException("ERROR! Invalid field access");		
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType left = this.left.typecheck(env);
		if(left instanceof recType){
			Binder b = ((recType)left).getBinding(id);
			if(b == null)
				throw new UndeclaredIDException("Missing field with id: "+ id);
			IType t = b.getExpression().getType();
			this.type = t;
			return t;
		}
		else if (left instanceof objType){
			this.type = ((objType)left).getType(id);
			return type;
		}
		throw new BadTypeException("Expected record or object!");

	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		IType type = left.getType();
		left.compile(block, env);
		if(type instanceof recType){
			recType rType = (recType)type;
			int offset = rType.getOffset(id);
			IType fType = rType.getBindingType(id);
			block.emit_load_record_field(rType,offset,fType);
		}
		else if(type instanceof objType){
			objType oType = (objType) type;
			int offset;
			IType fType;
			if(id.equals("this")){
				offset = 0;
				fType = type;
			}
			else{
				offset = oType.getOffset(id)+1;
				fType =oType.getType(id);
			}
			block.emit_get_field_from_object(oType,offset,fType);
		}
		else throw new BadTypeException("COMPILATION ERROR : Object or record expected!");
	}

	@Override
	public String toString(){
		return left.toString()+"."+id;
	}
}	
