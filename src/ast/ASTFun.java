package ast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import compUtils.CodeBlock;
import types.IType;
import types.funType;
import types.objType;
import values.*;
import environment.*;

public class ASTFun implements ASTNode {
	Map<String,IType> parameters;
	ASTNode body;
	IType type;
	public ASTFun(Map<String,IType> parameters, ASTNode body){
		this.parameters=parameters;
		this.body=body;	
	} 
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException,BadTypeException{
		List<String> params = new LinkedList<>(parameters.keySet());
		return new Closure(params,body,env);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {

		Environment<IType> local = env.beginScope();
		List<IType> types = new ArrayList<>();
		for(String s: parameters.keySet()){
			IType t = parameters.get(s);
			local.associate(s,t);
			types.add(t);
		}
		IType returnType = body.typecheck(local);
		this.type = new funType(types, returnType);
		return type;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		funType t = (funType)type;
		int frame_n = block.getCurrentFrame() == null ? 1:block.getCurrentFrame().number;


		for(IType type : parameters.values()){
			if(type instanceof funType)
				block.createClosureInterface((funType)type);
		}

		if(body.getType() instanceof funType)
			block.createClosureInterface((funType)body.getType());
		else if(body.getType() instanceof objType)
			block.emit_create_object((objType)body.getType(),frame_n);
		block.createClosureInterface(t);
		String closure_name = block.createClosure(t,frame_n,parameters.size());
		t.setLabel(closure_name);
		block.mapClosureToFrame(closure_name,frame_n);
		frame_n = block.createFrame(parameters.size());
		block.init_Closure_frame(frame_n,closure_name);
		CompileEnviroment<IValue> new_env;
		int i = 0;
		for(String id : parameters.keySet()){
			IType current_type = parameters.get(id);
			int offset = env.addFrame(id);
			block.emit_empty_line();
			block.emit_comment("Load variable frame_"+frame_n+"/"+id);
			block.emit_dup();
			block.emit_loadParameter(i+1,current_type);
			block.addTypeToFrame(current_type,i,frame_n);
			block.emit_store(frame_n,offset,current_type);
			i++;
		}
		new_env = env.beginScope();
		block.emit_push_frame(frame_n);
		body.compile(block,new_env);
		block.emit_return(body.getType());
		block.silent_popFrame();
		block.finishClosure();
		block.emit_function_creation(frame_n,closure_name);
	}

	@Override
	public String toString(){
		return "fun "+parameters.toString()+" => " + body.toString()+ " end";
	}
}
