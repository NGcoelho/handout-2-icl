package ast;
import compUtils.CodeBlock;
import compUtils.VarLoc;
import types.IType;
import values.*;
import environment.*;

public class ASTId implements ASTNode{
	
	private String id;
	private  IType type;

	public ASTId(String t1){
		id=t1;
		
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		return env.find(id);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		type = env.find(id);
		return type;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		VarLoc vl = env.doLookup(id);
		block.emit_comment("Getting var "+ id + " jumps" + vl.frame);
		block.emit_idval(vl.frame,vl.offset,type);
	}

	@Override
	public String toString(){ return id;}



}
