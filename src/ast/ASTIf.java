package ast;

import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.boolType;
import types.intType;
import values.*;

public class ASTIf implements ASTNode {
	ASTNode ifs, thens, elss;
	IType type;
	public ASTIf(ASTNode cond, ASTNode t, ASTNode e){
		ifs=cond; //if statement
		thens=t; //then statement
		elss=e;
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException,DuplicateIDException,BadTypeException{
		IValue v = ifs.eval(env);
		if( v instanceof boolValue) {
			boolean value = ((boolValue)v).getValue();
			if(value) return thens.eval(env);
			else return elss.eval(env);
		}

		throw new BadTypeException("Invalid type, expected boolean");
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType condition = this.ifs.typecheck(env);
		if(condition instanceof boolType){
			IType then = this.thens.typecheck(env);
			IType els = this.elss.typecheck(env);
			if(then.getInstance() == els.getInstance()){
				type = then;
				return then;
			}
			else if(then.getInstance() instanceof  boolType && els.getInstance() instanceof intType){
				type = els;
				return els;
			}
			else throw new BadTypeException("Unexpected expression type");
		}
		else throw new BadTypeException("Expected boolean!");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		ifs.compile(block, env);
		String label = block.emit_if_condition();
		label = block.emit_else(label);
		thens.compile(block, env);
		String end = block.emit_end();
		block.emit_then(label,end);
		elss.compile(block, env);
		block.finishIf(end);
	}

	@Override
	public String toString(){return "if "+ifs.toString()+" then " + thens.toString() + " else "+ elss.toString();}
}
