package ast;

import java.util.List;

import compUtils.CodeBlock;
import types.IType;
import values.*;
import environment.*;

public class ASTLet implements ASTNode{
	List<Binder> lets;
	ASTNode expression;
	IType type;

	public ASTLet(List<Binder> binders, ASTNode expr){
		lets=binders;
		expression=expr;	
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue value;
		Environment<IValue> newEnv = env.beginScope();
		for(Binder let: lets){
			IValue idValue = let.getExpression().eval(env);
			newEnv.associate(let.getID(), idValue);		
		}
		value = expression.eval(newEnv);
		newEnv.endScope();
		return value;	
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		Environment<IType> local = env.beginScope();
		IType type;
		for(Binder b: lets){
			type = b.getExpression().typecheck(local);
			local.associate(b.getID(),type);
		}
		IType type2 = expression.typecheck(local);
		this.type=type2;
		local.endScope();
		return type2;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		int frame_n = block.createFrame(lets.size());
		block.emit_empty_line();
		block.emit_comment("Init frame "+frame_n);
		block.init_frame(frame_n);
		CompileEnviroment<IValue> new_env;

		for (int i = 0; i < lets.size() ; i++) {
			Binder let = lets.get(i);
			String id = let.getID();
			int offset = env.addFrame(id);
			block.emit_empty_line();
			block.emit_comment("Load variable frame_"+frame_n+"/"+id);
			block.emit_dup();
			let.getExpression().compile(block,env.dupEnvironment());
			block.addTypeToFrame(let.getExpression().getType(),i,frame_n);
			block.emit_store(frame_n,offset,let.getExpression().getType());
		}
		new_env = env.beginScope();
		block.emit_push_frame(frame_n);
		block.emit_empty_line();
		block.emit_comment("Function [E]");
		expression.compile(block,new_env);

		block.emit_empty_line();
		block.emit_comment("Finish frame"+frame_n);
		block.emit_pop_frame(frame_n);
		new_env.endScope();
	}

	@Override
	public String toString(){
		String string="";
		for(Binder let: lets)
			string+=let.getID() + "=" + let.getExpression().toString()+" ";
		return "let " + string + " in " + expression.toString() + " end";
	}
}
