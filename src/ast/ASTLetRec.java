package ast;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import compUtils.CodeBlock;
import environment.*;
import javafx.util.Pair;
import org.omg.CORBA.COMM_FAILURE;
import types.IType;
import values.*;

public class ASTLetRec implements ASTNode{
	Map<String, Pair<IType, ASTNode>> bindings;
	ASTNode body;
	IType type;

	public ASTLetRec(Map<String, Pair<IType, ASTNode>> bindings, ASTNode body){
		this.bindings=bindings;
		this.body=body;
	}
	@Override
	public String toString(){
		String string="";
		for(Entry<String, Pair<IType,ASTNode>> e: bindings.entrySet()){
			string+= e.getKey() + " = " + e.getValue().getValue().toString()+ " ";
		}
		return "letrec " +string+" in " +body.toString() + " end";	
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException {
		
		Environment<IValue> localenv = env.beginScope();
	
		for(Entry<String, Pair<IType,ASTNode>> e: bindings.entrySet()){
			IValue value = e.getValue().getValue().eval(localenv);
			localenv.associate(e.getKey(),value);
		}
		IValue v = body.eval(localenv);
		localenv.endScope();
		return v;
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		Environment<IType> local = env.beginScope();
		for (Entry<String, Pair<IType,ASTNode>> e : bindings.entrySet())
			local.associate(e.getKey(),e.getValue().getKey());

		for(Pair<IType,ASTNode> p :bindings.values()){
			ASTNode n = p.getValue();
			if(n instanceof  ASTObject)
				((ASTObject)n).setType(p.getKey());
			n.typecheck(local);
		}
		IType t = body.typecheck(local);
		this.type = t;
		return type;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		int frame_n1 = block.createFrame(bindings.size());
		block.emit_empty_line();
		block.emit_comment("Init frame " + frame_n1);
		block.init_frame(frame_n1);
		block.emit_dup();
		block.emit_push_frame(frame_n1);
		CompileEnviroment<IValue> new_env = env.beginScope();
		int[] slots = new int[bindings.size()];
		int i = 0;
		for(String id : bindings.keySet()){
			int offset = env.addFrame(id);
			slots[i++] = offset;
		}
		i=0;
		for(Entry<String,Pair<IType,ASTNode>> e : bindings.entrySet()){
			block.emit_dup();
			Pair<IType,ASTNode> right = e.getValue();
			right.getValue().compile(block,new_env.dupEnvironment());
			block.addTypeToFrame(right.getValue().getType(),slots[i],frame_n1);
			block.emit_store(frame_n1,slots[i++],right.getValue().getType());
		}
		block.emit_push_frame(frame_n1);
		body.compile(block, new_env);
		block.emit_pop_frame(frame_n1);
		new_env.endScope();
	}

}
