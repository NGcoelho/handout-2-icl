package ast;
import compUtils.CodeBlock;
import environment.*;
import types.IType;
import values.IValue;

public interface ASTNode{
	IValue eval(Environment<IValue> env) throws UndeclaredIDException,DuplicateIDException,BadTypeException;

	IType getType();

	IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException;

	void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException;
	
	
}
