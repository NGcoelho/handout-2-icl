package ast;
import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.boolType;
import values.*;

public class ASTNot implements ASTNode{
	ASTNode val;
	IType type;
	
	public ASTNot(ASTNode val){
		this.val=val;
	}
	@Override
	public String toString(){
		return "!("+val.toString()+")";	
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue value= val.eval(env);
		if( value instanceof boolValue )
			return new boolValue(!((boolValue)value).getValue());
		throw new BadTypeException("Invalid type, expected boolean");
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType type = val.typecheck(env);
		if(type instanceof boolType){
			this.type = type;
			return type;
		}
		throw new BadTypeException("Expected booleans!");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		val.compile(block, env);
		block.emit_not();
	}

}
