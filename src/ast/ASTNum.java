package ast;
import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.intType;
import values.*;


public class ASTNum implements ASTNode{

	private int num;
	private IType type;
	public ASTNum(int t1){
		num=t1;
		
	}
	@Override
	public IValue eval(Environment<IValue> env) throws BadTypeException{
		return new intValue(num);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		type = intType.instance;
		return intType.instance;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException {
		block.emit_push(num);
	}

	@Override
	public String toString(){
		return Integer.toString(num);	
	}
}


