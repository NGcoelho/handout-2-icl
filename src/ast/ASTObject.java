package ast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import compUtils.CodeBlock;
import types.IType;
import types.objType;
import values.*;
import environment.*;

public class ASTObject implements ASTNode{
	List<Binder> bindings;
	private IType type;

	public ASTObject(List<Binder> bindings){
		this.bindings = bindings;	
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		Environment<IValue> newEnv = env.beginScope();
		Map<String,IValue> objectValues = new HashMap<String,IValue>();
		for(Binder b : bindings) {
			IValue value = b.getExpression().eval(env);
			objectValues.put(b.getID(), value);
		}


		ObjectValue v= new ObjectValue(objectValues, newEnv);
		newEnv.associate("this",v);
		newEnv.endScope();
		return v;
		
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		if(this.type == null){
			Environment<IType> local = env.beginScope();
			objType o = new objType();
			local.associate("this",o);
			for(Binder b : bindings){
				IType t = b.getExpression().typecheck(local);
				local.associate(b.getID(),t);
				o.addType(b.getID(),t);
			}
			this.type = o;
			local.endScope();
		}
		else{
			Environment<IType> local = env.beginScope();
			local.associate("this",type);
			for(Binder b : bindings){
				IType t = b.getExpression().typecheck(local);
				local.associate(b.getID(),t);
			}
			local.endScope();
		}
		return type;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		int frame_n1 = block.createFrame(bindings.size()+1);
		block.emit_empty_line();
		block.emit_comment("Init frame "+frame_n1);
		block.init_frame(frame_n1);
		block.emit_dup();
		block.emit_push_frame(frame_n1);

		block.emit_empty_line();
		block.emit_comment("Creating object");
		objType oType = (objType)type;
		block.emit_create_object(oType,frame_n1);
		block.addTypeToFrame(oType,env.addFrame("this"),frame_n1);
		block.emit_load_object_frame(oType,frame_n1);

		CompileEnviroment<IValue> new_env = env.beginScope();
		int[] slots = new int[bindings.size()];
		int i = 0;
		for(Binder b : bindings){
		 	int offset = env.addFrame(b.getID());
		 	slots[i++] = offset;
		}
		i = 0;
		for(Binder e : bindings){
			block.emit_get_object_frame(frame_n1);
			e.getExpression().compile(block, new_env.dupEnvironment());
			IType type = e.getExpression().getType();
			block.addTypeToFrame(type, slots[i],frame_n1);
			block.emit_put_object_field(oType,slots[i++],type);
		}
		block.emit_pop_frame(frame_n1);
		new_env.endScope();
	}

	@Override
	public String toString(){
		String value="";
		for(Binder bind : bindings)
			value+=" "+bind.getID()+" = "+bind.getExpression().toString()+" ";
		return "{{"+value+"}}";
	}

    public void setType(IType key) {
		this.type=key;
    }
}
