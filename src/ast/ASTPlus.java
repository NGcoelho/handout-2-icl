package ast;
import compUtils.CodeBlock;
import types.IType;
import types.intType;
import values.IValue;
import values.ReferenceValue;
import values.intValue;
import environment.*;

public class ASTPlus implements ASTNode{

	private ASTNode left, right;
	private IType type;

	public ASTPlus(ASTNode t1, ASTNode t2){
		left=t1;
		right=t2;
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue l=left.eval(env);
		IValue r=right.eval(env);
		if(l instanceof intValue && r instanceof intValue)
			return new intValue(((intValue)l).getValue() + ((intValue)r).getValue());
		else if(l instanceof ReferenceValue && r instanceof intValue){
			IValue int_l = ((ReferenceValue)l).getValue();
				if( int_l instanceof  intValue)
					return new intValue(((intValue)int_l).getValue() + ((intValue)r).getValue());


		}
		else if(l instanceof intValue && r instanceof ReferenceValue) {
			IValue int_r = ((ReferenceValue) r).getValue();
			if (int_r instanceof intValue)
				return new intValue(((intValue) l).getValue() + ((intValue) int_r).getValue());

		}

		throw new BadTypeException("Invalid type, expected numbers");
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType left = this.left.typecheck(env);
		IType right = this.right.typecheck(env);

		if(left instanceof intType && right instanceof intType){
			type=left;
			return left;
		}

		throw new BadTypeException("Expected numbers ");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, BadTypeException, UndeclaredIDException {
		left.compile(block,env);
		right.compile(block,env);
		block.emit_add();
	}

	@Override
	public String toString() {
		return "("+left.toString()+"+"+right.toString()+")";
	}

	
}


