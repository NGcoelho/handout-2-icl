package ast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import compUtils.CodeBlock;
import types.IType;
import types.recType;
import values.*;
import environment.*;

public class ASTRecord implements ASTNode{
	List<Binder> bindings;
	IType type;

	public ASTRecord(List<Binder> bindings){
		this.bindings = bindings;	
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		Map<String,IValue> recordValues = new HashMap<String,IValue>();
		for(Binder b : bindings) {
			IValue v = b.getExpression().eval(env);
			recordValues.put(b.getID(), v);		
		}
		return new RecordValue(recordValues);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		recType r = new recType(bindings);
		for(Binder b : bindings){
			b.getExpression().typecheck(env);
		}
		this.type= r;
		return r;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		recType rType = (recType) type;
		block.emit_create_record(rType);
		for(Binder b : bindings){
			String id = b.getID();
			block.emit_dup();
			b.getExpression().compile(block, env);
			block.emit_put_record_field(rType,id);
		}
	}

	@Override
	public String toString(){
		String value="";
		for(Binder bind : bindings)
			value+=" "+bind.getID()+" = "+bind.getExpression().toString()+" ";
		return "{"+value+"}";
	}
}
