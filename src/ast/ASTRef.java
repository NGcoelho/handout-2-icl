package ast;

import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.refType;
import values.*;

public class ASTRef implements ASTNode{
	ASTNode value;
	IType type;
	
	public ASTRef(ASTNode value){
		this.value=value;	
	}

	@Override
	public String toString(){
		return value.toString();	
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		return new ReferenceValue(value.eval(env));	
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		type = new refType(value.typecheck(env));
		return type;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		String cell = block.emit_create_cell(this.getType());
		block.emit_dup();
		value.compile(block, env);
		block.emit_put_field(value.getType(),cell);
	}
}
