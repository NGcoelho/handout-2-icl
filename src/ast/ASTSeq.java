package ast;
import compUtils.CodeBlock;
import types.IType;
import values.IValue;
import values.*;
import environment.*;

public class ASTSeq implements ASTNode{

	private ASTNode left, right;
	private IType type;
	public ASTSeq(ASTNode t1, ASTNode t2){
		left=t1;
		right=t2;
	}
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue l=left.eval(env);
		IValue r=right.eval(env);
		return r;
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType left = this.left.typecheck(env);
		IType right = this.right.typecheck(env);
		type = right;
		return right;
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		left.compile(block, env);
		block.emit_pop();
		right.compile(block,env);
	}

	@Override
	public String toString() {
		return "("+left.toString()+" ; "+right.toString()+")";
	}

	
}
