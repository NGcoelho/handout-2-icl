package ast;

import compUtils.CodeBlock;
import environment.*;
import types.IType;
import types.boolType;
import values.*;

public class ASTWhile implements ASTNode{
	ASTNode condition, expression;
	IType type;
	
	public ASTWhile(ASTNode condition, ASTNode expression){
		this.condition=condition;
		this.expression=expression;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIDException, DuplicateIDException, BadTypeException{
		IValue cond = condition.eval(env);
		if(cond instanceof boolValue){
			boolean b=((boolValue)cond).getValue();
			if(b){
				IValue v = expression.eval(env);
				return this.eval(env);
			}
			else return new boolValue(false);		
		}
		throw new BadTypeException("Invalid condtion, expected boolean");
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIDException, BadTypeException, DuplicateIDException {
		IType type = condition.typecheck(env);
		if(type instanceof boolType){
			IType t2 = expression.typecheck(env);
			this.type = boolType.instance;
			return boolType.instance;
		}
		throw new BadTypeException("Wrong types in while!");
	}

	@Override
	public void compile(CodeBlock block, CompileEnviroment<IValue> env) throws DuplicateIDException, UndeclaredIDException, BadTypeException {
		String start = block.emit_label();
		block.emit_comment("Compiling while cond");
		condition.compile(block, env);
		String endlabel = block.emit_while_condition();
		block.emit_comment("Compiling while body");
		expression.compile(block, env);
		block.emit_pop();
		block.emit_end_while(start,endlabel);
	}

	@Override
	public String toString(){
		return "while " + condition.toString() + "do " + expression.toString() + " end";	
	}
}
