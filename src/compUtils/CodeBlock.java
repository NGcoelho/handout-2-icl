package compUtils;

import environment.Binder;
import environment.UndeclaredIDException;
import javafx.util.Pair;
import types.*;
import java.io.*;
import java.util.*;

public class CodeBlock {

    private static final boolean DEBUG = true;

    public static class StackFrame{
        public int number, nLets;
        StackFrame parent;
        static int fnumber = 1;
        boolean declaration;
        IType[] types;

       public StackFrame(int nLets, StackFrame parent){
            this.number=fnumber++;
            this.nLets=nLets;
            this.parent=parent;
            this.declaration=true;
            this.types=new IType[nLets];
        }

        public void setFieldType(int field, IType type){
            this.types[field]= type;
        }
        void finishDeclaration(){
            this.declaration = false;
        }
        void dump(){
            FileOutputStream outputStream;
            OutputStreamWriter writer;
            try{
                outputStream = new FileOutputStream("frame_"+this.number+".j");
                writer = new OutputStreamWriter(outputStream);

                //Writting header
                writer.write(".source frame_"+this.number+".j\n.class frame_"+this.number+"\n"
                    +".super java/lang/Object\n.implements frame\n\n");
                //have we got parent frame?
                if(this.parent!=null)
                    writer.write(".field public SL Lframe_"+this.parent.number+";\n");

                //field declarations
                for (int i = 0; i < this.nLets; i++) {
                    String type = CodeBlock.getTypeLabel(types[i]);
                    String locName = String.format("loc_%02d",i);
                    writer.write(".field public "+locName+" "+type+"\n");
                }

                //writting footer
                writer.write(".method public <init>()V\naload_0\ninvokespecial java/lang/Object/<init>()V\n"
                    + "return\n.end method");
                writer.close();
            }
            catch (IOException e){
                e.printStackTrace();

            }
        }
    }
    public ArrayList<String> code;
    public int labelCount = 0;
    private LinkedList<StackFrame> frames;
    private Stack<StackFrame> stack;
    private Set<IType> types;
    private static Map<funType, String> funTypeMapping;
    private static Map<String, Pair<IType,List<String>>> closureCodeMapping;
    private static Map<String,StackFrame> closureToFrameMapping;
    private static Map<recType,String> recordMapping;
    private static Map<objType,String> objectMapping;
    private static Map<objType,StackFrame> objectToFrameMapping;
    private static Map<String,List<String>> interfaceMapping;

    private Stack<ArrayList<String>> codeStack;

    public CodeBlock() {
        StackFrame.fnumber = 1;
        code = new ArrayList<>(100);
        frames = new LinkedList<>();
        stack = new Stack<>();
        codeStack = new Stack<>();
        types = new HashSet<>();
        funTypeMapping = new LinkedHashMap<>();
        closureCodeMapping = new LinkedHashMap<>();
        closureToFrameMapping = new LinkedHashMap<>();
        interfaceMapping = new LinkedHashMap<>();
        recordMapping = new LinkedHashMap<>();
        objectMapping = new LinkedHashMap<>();
        objectToFrameMapping = new LinkedHashMap<>();
    }
    //Le java assembley
    public void emit_push(int n){
        code.add("sipush "+n);
    }
    public void emit_add(){
        code.add("iadd");
    }
    public void emit_sub(){
        code.add("isub");
    }
    public void emit_mul(){
        code.add("imul");
    }
    public void emit_div(){
        code.add("idiv");
    }
    public void end_decl(){
        code.add("end method");
    }
    public void emit_eq(){
        String label_a = createLabel();
        String label_b = createLabel();
        code.add("if_icmpne "+label_a);
        emit_push(1);
        code.add("goto "+label_b);
        code.add(label_a+":");
        emit_push(0);
        code.add(label_b+":");

    }
    public void emit_not(){
        String label_a = createLabel();
        String label_b = createLabel();
        code.add("ifne "+ label_a);
        emit_push(1);
        code.add("goto " +label_b);
        code.add(label_a+":");
        emit_push(0);
        code.add(label_b+":");
    }
    public void emit_diff(){
        String label_a = createLabel();
        String label_b = createLabel();
        code.add("if_icmpeq "+label_a);
        emit_push(1);
        code.add("goto "+label_b);
        code.add(label_a+":");
        emit_push(0);
        code.add(label_b+":");

    }
    @SuppressWarnings("Duplicates")
    public void emit_gt(){
        String label_1 = createLabel();
        String label_2 = createLabel();
        code.add("if_icmple "+ label_1);
        emit_push(1);
        code.add("goto " +label_2);
        code.add(label_1+":");
        emit_push(0);
        code.add(label_2+":");
    }
    public void emit_gteq(){
        String label_a = createLabel();
        String label_b = createLabel();
        code.add("if_icmplt "+ label_a);
        emit_push(1);
        code.add("goto " +label_b);
        code.add(label_a+":");
        emit_push(0);
        code.add(label_b+":");
    }
    public void emit_sm(){
        String label_a = createLabel();
        String label_b = createLabel();
        code.add("if_icmpge "+ label_a);
        emit_push(1);
        code.add("goto " +label_b);
        code.add(label_a+":");
        emit_push(0);
        code.add(label_b+":");
    }
    @SuppressWarnings("Duplicates")
    public void emit_smeq(){
        String label_a = createLabel();
        String label_b = createLabel();
        code.add("if_icmple "+ label_a);
        emit_push(1);
        code.add("goto " +label_b);
        code.add(label_a+":");
        emit_push(0);
        code.add(label_b+":");
    }

    public void emit_and(){
        code.add("iand");
    }

    public void emit_or(){
        code.add("ior");
    }

    public void emit_dup() { code.add("dup"); }

    public  void emit_store(int frame_n, int offset, IType type){
        code.add("putfield frame_"+ frame_n+"/"+String.format("loc_%02d",offset)+getTypeLabel(type));
    }

    public void emit_push_frame(int frame_n){
        code.add("astore 0");
        StackFrame frame = getFrame(frame_n);
        frame.finishDeclaration();
        this.stack.push(frame);
    }

    //FOR LET REC ONLY
    public void emit_rec_push_frame(int frame_n){
        code.add("astore 0");
        StackFrame frame = getFrame(frame_n);
        frame.finishDeclaration();
    }

    public void emit_pop_frame(int frame_n){
        stack.pop();
        StackFrame frame = getFrame(frame_n);
        if(frame != null && frame.parent != null){
            code.add("aload 0");
            code.add("checkcast frame_"+frame.number);
            code.add("getfield frame_"+frame.number+"/SL Lframe_"+frame.parent.number+";");
            code.add("astore 0");
        }
        else{
            code.add("aconst_null");
            code.add("astore 0");
        }
    }

    public void emit_idval(int jumps, int offset, IType type){
        StackFrame frame = getCurrentFrame();
        code.add("aload 0");
        code.add("checkcast frame_"+frame.number);
        for(; jumps > 1; jumps --){
            code.add("getfield frame_"+frame.number+"/SL Lframe_"+frame.parent.number+";");
            frame = frame.parent;
        }
        String type_name = CodeBlock.getTypeLabel(type);
        code.add("getfield frame_"+ frame.number+"/loc_0"+offset+type_name);

    }

    public void emit_put_idval(IType type, IType val_type){
        IType current_type = type;
        if(current_type instanceof  refType){
            IType next_type = ((refType)current_type).type;
            while(next_type instanceof  refType){
                current_type = next_type;
                next_type= ((refType)current_type).type;
            }
        }
        String value_name = CodeBlock.getTypeLabel(val_type);
        code.add("putfield "+ current_type.getLabel() + "/value "+ value_name);
    }

    public void emit_assignReturnValue(IType type, IType val_type){
        IType current_type = type;
        if(current_type instanceof  refType){
            IType next_type =((refType)current_type).type;
            while(next_type instanceof  refType){
                current_type = next_type;
                next_type= ((refType)current_type).type;
            }
        }
        String value_name = CodeBlock.getTypeLabel(val_type);
        code.add("getfield "+ current_type.getLabel()+"/value "+value_name);
    }

    public void emit_pop(){
        code.add("pop");
    }

    public void emit_comment(String comment){
        if(DEBUG)
            code.add(";"+comment);
    }

    public void emit_empty_line(){
        if(DEBUG)
            code.add("");
    }

    public String emit_create_cell(IType type){
        String t = type.getLabel();
        if(!types.contains(type))
            types.add(type);
        code.add("new "+t);
        emit_dup();
        code.add("invokespecial "+t+"/<init>()V");
        return t;
    }

    public void emit_put_field(IType type, String cell){
        String value_type ;
        this.emit_comment("Type is: " + type.getLabel());
        if(type instanceof refType){
            value_type = "L"+type.getLabel()+";";
        }
        else value_type = type.getLabel();
        code.add("putfield "+cell+"/value "+value_type);
    }

    public void emit_get_value(IType type){
        if(type instanceof refType){
            IType subtype = ((refType)type).type;
            String type_label = CodeBlock.getCleanTypeLabel(type);
            String subtype_label = CodeBlock.getCleanTypeLabel(subtype);
            code.add("getfield "+type_label+"/value "+subtype_label);
        }
    }

    public String emit_if_condition(){
        emit_push(1);
        String label = this.createLabel();
        code.add("if_icmpeq "+label);
        return label;
    }

    public void emit_then(String else_label, String end){
        code.add("goto "+end);
        code.add(else_label+":");
    }

    public String emit_else(String label){
        String n_label = this.createLabel();
        code.add("goto "+n_label);
        code.add(label+":");
        return n_label;
    }

    public void finishIf(String end){
        code.add(end+":");
    }

    public String emit_end(){
        String label = this.createLabel();
        return label;
    }

    public String emit_label(){
        String label = this.createLabel();
        code.add(label+":");
        return label;
    }

    public String emit_while_condition(){
        emit_push(1);
        String end = createLabel();
        code.add("if_icmpne "+end);
        return end;
    }

    public void emit_end_while(String start, String end){
        code.add("goto "+start);
        code.add(end+":");
        emit_push(0);
    }

    public void emit_loadParameter(int i, IType type){
        if(type instanceof refType || type instanceof funType || type instanceof recType || type instanceof objType)
            code.add("aload "+i);
        else code.add("iload "+ i);
    }

    public void emit_return(IType t){
        if(t instanceof funType || t instanceof refType || t instanceof recType)
            code.add("areturn");
        else  code.add("ireturn");
        code.add(".end method");
    }

    public void emit_function_call(funType t, int argumentCount){
        String type = funTypeMapping.get(t);
        code.add("invokeinterface "+type+"/"+generateCallFun(t)+" "+(argumentCount +1));
    }

    public void emit_function_creation(int frame_n, String closure_name){
        code.add("new "+closure_name);
        emit_dup();
        code.add("invokespecial "+ closure_name + "/<init>()V");
        StackFrame frame = getFrame(frame_n);
        if(frame.parent != null){
            emit_dup();
            code.add("aload 0");
            code.add("putfield "+closure_name+"/SL Lframe_"+frame.parent.number+";");
        }
    }

    public void emit_checkcast(funType function){
        code.add("checkcast "+funTypeMapping.get(function));
    }

    public void emit_create_record(recType type){
        String record_name = mapRecord(type);
        code.add("new "+ record_name);
        emit_dup();
        code.add("invokespecial "+record_name+"/<init>()V");
    }

    public void emit_put_record_field(recType type, String id) throws UndeclaredIDException {
        String name = getRecordName(type);
        int offset = type.getOffset(id);
        IType bind_type = type.getBindingType(id);
        String type_s = getTypeLabel(bind_type);
        code.add("putfield "+ name + "/" +String.format("loc_%02d",offset)+type_s);
    }
    public void emit_load_record_field(recType type, int offset, IType ftype){
        String name = getRecordName(type);
        String type_s  = getTypeLabel(ftype);
        code.add("checkcast "+ name);
        code.add("getfield "+ name + "/"+ String.format("loc_%02d",offset)+type_s);
    }

    public void emit_create_object(objType type, int frame_n){
        String name;
        StackFrame frame = this.getFrame(frame_n);
        if(objectMapping.containsKey(type))
            name = objectMapping.get(type);
        else {
            name = "object_"+String.format("%02d", this.objectMapping.size());
            objectMapping.put(type,name);
            objectToFrameMapping.put(type,frame);
        }
        emit_dup();
        code.add("checkcast frame_"+frame_n) ;
        code.add("new "+name);
        emit_dup();
        code.add("invokespecial "+name+"/<init>()V");
        code.add("putfield frame_"+frame_n+"/loc_00 L"+name+";");
        code.add("getfield frame_"+frame_n+"/loc_00 L"+name+";");
    }

    public void emit_load_object_frame(objType type, int frame_n){
        String name = objectMapping.get(type);
        StackFrame frame = this.getFrame(frame_n);
        if(frame == null)
            throw new RuntimeException("Frame "+ frame_n + "doesn't exist !");
        this.objectToFrameMapping.put(type,frame);
        emit_dup();
        code.add("aload 0");
        code.add("checkcast frame_"+ frame_n);
        code.add("putfield " + name + "/frame Lframe_"+frame_n+";");
    }

    public void emit_put_object_field(objType type, int offset, IType ftype){
        StackFrame frame = objectToFrameMapping.get(type);
        String type_name = getTypeLabel(ftype);
        code.add("putfield frame_"+frame.number+"/"+String.format("loc_%02d",offset)+type_name);
    }
    public void emit_get_object_frame(int frame_n){
        code.add("aload 0");
        code.add("checkcast frame_"+ frame_n);
    }
    public void emit_load_object_field(objType type, int offset, IType ftype){
        String oName = this.objectMapping.get(type);
        StackFrame frame = this.objectToFrameMapping.get(type);
        code.add("aload 0");
        code.add("checkcast frame_"+frame.number);
        String type_name = getTypeLabel(ftype);
        code.add("getfield frame_"+frame.number+"/"+String.format("loc_%02d",offset)+type_name);
    }

    public void emit_get_field_from_object(objType type, int offset ,IType ftype){
        String o_name = this.objectMapping.get(type);
        StackFrame frame = this.objectToFrameMapping.get(type);
        code.add("getfield "+ o_name+ "/frame Lframe_"+frame.number+";");
        code.add("checkcast frame_"+frame.number);
        String type_name = getTypeLabel(ftype);
        code.add("getfield frame_"+frame.number+"/"+String.format("loc_%02d",offset)+type_name);
    }

    public void silent_popFrame(){
        this.stack.pop();
    }

    private String createLabel() {
        return "L"+labelCount++;
    }

    public String createClosureInterface(funType function){
        if(this.funTypeMapping.containsKey(function))
            return this.funTypeMapping.get(function);
        String type = "type_" + String.format("%02d",this.funTypeMapping.size());
        List<String> head = new ArrayList<>(5);
        head.add(".source " + type +".j\n");
        head.add(".interface public "+type+"\n");
        head.add(".super java/lang/Object \n");
        String parameters="";
        for(IType t : function.getParamTypes()){
            if(t instanceof funType)
                parameters = parameters + "L" + funTypeMapping.get(t) +";";
            else if (t instanceof refType)
                parameters = parameters +"L" + t.getLabel()+";";
            else if(t instanceof objType)
                parameters += "L" + this.getCleanTypeLabel(t)+";";
            else parameters = parameters + t.getLabel();
        }

        IType returnType = function.getReturnType();
        String ret ="";
        if(returnType instanceof funType)
            ret="L"+funTypeMapping.get(returnType)+";";
        else if (returnType instanceof  refType)
            ret = "L"+ returnType.getLabel()+";";
        else if (returnType instanceof  objType)
            ret = "L"+objectMapping.get(returnType)+";";
        else ret = returnType.getLabel();

        head.add(".method public abstract call("+parameters+")"+ret+"\n");
        head.add(".end method");

        this.funTypeMapping.put(function,type);
        System.out.println("Declare interface " +type+" => "+function.hashCode());
        this.interfaceMapping.put(type,head);
        return type;
    }

    public String createClosure(funType type, int nFrame, int parametersCount){
        String closureName = "closure_"+ String.format("%02d",this.closureCodeMapping.size());
        ArrayList<String> newCode = new ArrayList<>();
        newCode.add(".source "+closureName+".j");
        newCode.add(".class public " +closureName);
        newCode.add(".super java/lang/Object \n");
        newCode.add(".implements "+ funTypeMapping.get(type));
        if(nFrame > 0)
            newCode.add(".field public SL Lframe_"+nFrame+";");
        newCode.add(".method public <init>()V");
        newCode.add("aload_0");
        newCode.add("invokespecial java/lang/Object/<init>()V");
        newCode.add("return");
        newCode.add(".end method");
        newCode.add(".method public " + generateCallFun(type));
        //remember :0 -this, 1 to n-1 parameters; n- SL
        newCode.add(".limit locals "+ (parametersCount+2));
        newCode.add(".limit stack 256");
        codeStack.push(code);
        code = newCode ;
        closureCodeMapping.put(closureName,new Pair<>(type, newCode));
        return closureName;
    }

    public int createFrame(int size, int parent){
        StackFrame frame = this.getFrame(parent);
        frame = new StackFrame(size, frame);
        frames.add(frame);
        code.add("new frame_"+frame.number);
        emit_dup();
        code.add("invokespecial frame_"+frame.number+"/<init>()V");
        return frame.number;
    }

    public int createFrame(int size){
        StackFrame frame = this.getCurrentFrame();
        if (frame == null)
            frame = new StackFrame(size,null);  //FRAMES SEM PAI (coitadas :c)
        else if(!frame.declaration)
            frame = new StackFrame(size,this.getCurrentFrame());
        else frame = new StackFrame(size,this.getCurrentFrame().parent);

        frames.add(frame);
        code.add("new frame_"+frame.number);
        emit_dup();
        code.add("invokespecial frame_"+frame.number+"/<init>()V");
        return frame.number;
    }

    public int getParentFrame(){
        try{
            StackFrame current = stack.peek();
            if(current.parent != null)
                return current.parent.number;
             return -1;

        }catch(EmptyStackException e){
            e.printStackTrace();
            return -1;
        }
    }

    public StackFrame getCurrentFrame() {
        try{
            return stack.peek();
        }catch(EmptyStackException e){
            return null;
        }
    }

    public StackFrame getFrame(int frame_n){
        for (int i = 0; i <frames.size() ; i++)
            if(frames.get(i).number == frame_n)
                return frames.get(i);
         //ideally null should not be returned
        return null;
    }
    public void init_frame(int frame_n){
        StackFrame frame = getFrame(frame_n);
        if(frame.parent != null){
            emit_dup();
            code.add("aload 0");
            code.add("putfield frame_"+frame_n+"/SL Lframe_"+frame.parent.number+";");
        }
    }

    public void init_Closure_frame(int frame_n, String closureName){
        StackFrame frame = getFrame(frame_n);
        if(frame.parent!=null){
            emit_dup();
            code.add("aload 0");
            code.add("getfield "+ closureName + "/SL Lframe_"+frame.parent.number+";");
            code.add("putfield frame_"+frame_n+"/SL Lframe_"+frame.parent.number+";");
        }
    }

    public void addTypeToFrame(IType type, int num, int frame_n){
        for(StackFrame frame: frames){
            if(frame.number == frame_n){
                frame.setFieldType(num,type);
                break;
            }
        }
    }

    public void finishClosure(){
        code = codeStack.pop();
    }

    public void mapClosureToFrame(String closure_name, int frame_n){
        this.closureToFrameMapping.put(closure_name,this.getFrame(frame_n));
    }
    public StackFrame getClosureStackFrame(String closure_name){
        return this.closureToFrameMapping.get(closure_name);
    }
    public void putClosureFrame(String typeLabel){
        StackFrame frame = getClosureStackFrame(typeLabel);
        code.add("aload 0");
        code.add("putfield "+typeLabel+"/SL frame_"+frame.number+";");
    }


    private String mapRecord(recType record) {
        if(this.recordMapping.containsKey(record))
            return recordMapping.get(record);
        String name = "record_" + String.format("%02d",this.recordMapping.size());
        this.recordMapping.put(record,name);
        return name;
    }

    public String getRecordName(recType record){
        if(this.recordMapping.containsKey(record))
            return recordMapping.get(record);
        return null;
    }

    //ONLY FOR LET RECS
    public void rec_push_frame(int frame_n){
        StackFrame frame = getFrame(frame_n);
        this.stack.push(frame);
    }

    //------------------------------LET THERE BE DUMPAGE------------------------------------


    void dump_header(PrintStream out){
        out.println(".class public Demo");
        out.println(".super java/lang/Object");
        out.println("");
        out.println(";");
        out.println("; standard initializer");
        out.println(".method public <init>()V");
        out.println("   aload_0");
        out.println("   invokenonvirtual java/lang/Object/<init>()V");
        out.println("   return");
        out.println(".end method");
        out.println("");
        out.println(".method public static main([Ljava/lang/String;)V");
        out.println("       ; set limits used by this method");
        out.println("       .limit locals 10");
        out.println("       .limit stack 256");
        out.println("");
        out.println("       ; setup local variables:");
        out.println("");
        out.println("       ;   1 - the PrintStream object held in java.lang.out");
        out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
        out.println("");
        out.println("       ; place your bytecodes here");
        out.println("       ; START");
        out.println("");
    }

    void dump_code(PrintStream out){
        for (String line : code) {
            out.println("       "+ line);
        }
    }

    void dump_footer(PrintStream out){
        out.println("       ; END");
        out.println("");
        out.println("       ; convert to String;");
        out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
        out.println("       ; call println ");
        out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        out.println("");
        out.println("       return");
        out.println("");
        out.println(".end method");
    }


    private void dump_frame_interface(){
        File f = new File("frame.j");
        try{
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f));
            writer.write(".source frame.j\n.interface public frame\n.super java/lang/Object");
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    private void dump_frames(){
        dump_frame_interface();
        for (StackFrame f : frames) {
            f.dump();
        }
    }

    private void dump_types(){
        for (IType type : this.types) {
            String filename = type.getLabel()+".j";
            File f = new File(filename);
            String value_type;
            if(type instanceof refType){
                IType sub_type = ((refType)type).type;
                if(sub_type instanceof refType)
                    value_type = "L" + sub_type.getLabel() +";";
                else value_type = sub_type.getLabel();
            }
            else value_type = type.getLabel();

            try{
                OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f));
                writer.write(".source "+filename+"\n.class "+type.getLabel()+"\n"
                    +".super java/lang/Object\n\n.field public value " + value_type+"\n"
                    +".method public <init>()V\naload_0\ninvokespecial java/lang/Object/<init>()V\n"
                    +"return\n.end method\n");
                writer.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private void dump_objects(){
        for(Map.Entry<objType, String> e : objectMapping.entrySet()){
            String object_name = e.getValue();
            objType object = e.getKey();
            File f = new File(object_name+".j");
            try{
                OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f));
                //header
                writer.write(".source "+object_name+".j\n");
                writer.write(".class "+object_name +"\n");
                writer.write(".super java/lang/Object\n\n");
                //frame declaration
                StackFrame frame = objectToFrameMapping.get(object);
                writer.write(".field public frame Lframe_"+frame.number+";\n");
                writer.write("\n");
                writer.write(".method public <init>()V\naload_0\ninvokespecial java/lang/Object/<init>()V\n" +
                        "return\n.end method\n");
                writer.close();
            }catch (IOException p){
                p.printStackTrace();
            }
        }
    }

    private void dump_records(){
        for(Map.Entry<recType,String> e : recordMapping.entrySet()){
            String record_name = e.getValue();
            recType type = e.getKey();
            File f = new File(record_name+".j");

            try {
                OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f));
                //header
                writer.write(".source "+record_name+".j\n");
                writer.write(".class "+record_name+"\n");
                writer.write(".super java/lang/Object\n\n");
                //fields declaration
                for(Binder b : type.bindings){
                    int offset = type.getOffset(b.getID());
                    String loc_name = String.format("loc_%02d",offset);
                    IType b_type = b.getExpression().getType();
                    String type_s = getTypeLabel(b_type);
                    writer.write(".field public "+loc_name+type_s+"\n");
                }

                writer.write("\n");
                writer.write(".method public <init>()V\naload_0\ninvokespecial java/lang/Object/<init>()V\n"
                        +"return\n.end method\n");
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (UndeclaredIDException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void dump_closures(){
        for(Map.Entry<String,Pair<IType,List<String>>> e : closureCodeMapping.entrySet()){
            String filename = e.getKey()+".j";
            File f = new File(filename);
            try {
                OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f));
                List<String> text = e.getValue().getValue();
                for (String line : text)
                    writer.write(line + "\n");
                writer.close();
            }catch (IOException e1){
                e1.printStackTrace();
            }
        }
    }

    private void dump_interfaces(){
        for(Map.Entry<String,List<String>> e : interfaceMapping.entrySet()){
            String filename = e.getKey()+".j";
            File f = new File(filename);
            try{
                OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f));
                for(String line : e.getValue())
                    writer.write(line);
                writer.close();
            }catch(IOException e1){
                e1.printStackTrace();
            }
        }
    }

    public void dump(String filename) throws FileNotFoundException{
        dump_objects();
        dump_records();
        dump_frames();
        dump_types();
        dump_interfaces();
        dump_closures();
        PrintStream out= new PrintStream(new FileOutputStream(filename));
        dump_header(out);
        dump_code(out);
        dump_footer(out);
    }


    public static String getTypeLabel(IType type) {
        String types = " ";
        if(type instanceof  intType || type instanceof  boolType)
            types+="I";
        else if(type instanceof funType)
            types+="L"+funTypeMapping.get(type)+";";
        else if(type instanceof refType)
            types+="L"+type.getLabel()+";";
        else if (type instanceof recType)
            types+="L" + recordMapping.get(type)+";";
        else if(type instanceof objType)
            types+="L"+objectMapping.get(type)+";";

        else throw new RuntimeException("Error in getTypeLabel!");

        return types;
    }

    private String generateCallFun(funType type) {
        String args = "";
        for (IType t : type.getParamTypes()) {
            if (t instanceof funType)
                args += "L" + this.funTypeMapping.get(t) + ";";
            else if (t instanceof refType)
                args += "L" + t.getLabel() + ";";
            else if (t instanceof objType)
                args += "L" + objectMapping.get(t) + ";";
            else args += t.getLabel();
        }
        if (type.getReturnType() instanceof funType) {
            String type_n = funTypeMapping.get(type.getReturnType());
            return  "call("+args+")L"+type_n+";";
        }
        else return "call("+args+")"+type.getReturnType().getLabel();
    }

    private static String getCleanTypeLabel(IType t) {
        if(t instanceof  intType || t instanceof  boolType)
            return "I";
        else if(t instanceof funType)
            return funTypeMapping.get(t);
        else if (t instanceof refType)
            return t.getLabel();
        else if (t instanceof recType)
            return recordMapping.get(t);
        else if (t instanceof  objType)
            return objectMapping.get(t);

        else throw new RuntimeException("Error in getCleanTypeLabel!");
    }
}
