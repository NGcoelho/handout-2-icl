package compUtils;

public class VarLoc {

    public int frame;
    public int offset;

    public VarLoc(int frame, int offset){
        this.frame=frame;
        this.offset=offset;
    }

}
