package console;

import ast.ASTNode;
import compUtils.CodeBlock;
import environment.*;
import parser.ParseException;
import parser.Parser;
import types.IType;
import values.IValue;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class CompilerShell {
    public static void main(String args[]){
        Parser parser = new Parser(System.in);
        ASTNode expression;
        System.out.println("Started Compiler. Type exit if you want to leave.");
        while(true) {
            try {
                 expression = parser.Start();
                 CodeBlock code = new CodeBlock();
                 CompileEnviroment<IValue> env = new CompileEnviroment<>();
                 Environment<IType> t_env = new Environment<>();
                 System.out.println("Got: " + expression.toString() );
                 IType expression_type = expression.typecheck(t_env);
                 System.out.println("Typecheck OK! ---> Type: " + expression_type.toString());
                 expression.compile(code, env);
                 code.dump("Demo.j");
                 System.out.println("Compiled!");

            } catch (Exception e) {
                 System.out.println("Syntax Error!");
                 System.out.println(e.getMessage());
                 e.printStackTrace();
                 parser.ReInit(System.in);
             }

        }

    }

    public static void compile(String expression_s) throws ParseException, DuplicateIDException, BadTypeException, UndeclaredIDException, FileNotFoundException {
        Parser parser = new Parser(new ByteArrayInputStream(expression_s.getBytes()));
        System.out.println("Started Compiler.");
        ASTNode expression = parser.Start();
        CodeBlock code = new CodeBlock();
        CompileEnviroment<IValue> env = new CompileEnviroment<>();
        Environment<IType> t_env = new Environment<>();
        System.out.println("Got: " + expression.toString() );
        IType expression_type = expression.typecheck(t_env);
        System.out.println("Typecheck OK! ---> Type: " + expression_type.toString());
        expression.compile(code, env);
        code.dump("Demo.j");
        System.out.println("Compiled!");
    }
}
