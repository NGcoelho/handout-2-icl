package console;

import ast.ASTNode;
import environment.*;
import parser.ParseException;
import parser.Parser;
import types.IType;
import values.*;

import java.io.ByteArrayInputStream;

public class shell {
    public static void main(String args[]){
        Parser parser = new Parser(System.in);
        System.out.println("Started Parser...");
        while(true){
            try{
                ASTNode n = parser.Start();
                Environment<IValue> env = new Environment<>();
                Environment<IType> typeEnvironment = new Environment<>();
                System.out.println("Got: " + n.toString() );
                IType expression_type = n.typecheck(typeEnvironment);
                System.out.println("Typecheck OK! ---> Type: "+ expression_type.toString());
                System.out.println("Result: " + n.eval(env));

            }catch (ParseException e){
                System.out.println("Syntax Error!");
                e.printStackTrace();
                parser.ReInit(System.in);
            } catch (UndeclaredIDException e) {
                e.printStackTrace();
                parser.ReInit(System.in);
            } catch (BadTypeException e) {
                e.printStackTrace();
                parser.ReInit(System.in);
            } catch (DuplicateIDException e) {
                e.printStackTrace();
                parser.ReInit(System.in);
            }
        }
    }
    public static boolean acceptCompare(String expression, IValue value ){
        Parser parser = new Parser(new ByteArrayInputStream(expression.getBytes()));
        try{
            ASTNode node=parser.Start();
            Environment<IValue> environment = new Environment<>();
            IValue result = node.eval(environment);
            if(result instanceof intValue && value instanceof intValue){
                return ((intValue)result).getValue() == ((intValue)value).getValue();
            }
            else if(result instanceof boolValue && value instanceof boolValue){
                return ((boolValue)result).getValue() == ((boolValue)value).getValue();
            }
            return false;

        }catch(Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static boolean accept(String expression){
        Parser parser = new Parser(new ByteArrayInputStream(expression.getBytes()));
        try{
            ASTNode node=parser.Start();
            Environment<IValue> environment = new Environment<>();
            node.eval(environment);
            return true;
        }catch(Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
}
