package environment;

public class BadTypeException extends Exception {
	
	private static final long serialVersionUID = 1L;	
	
	private String id;

	public BadTypeException(String id) {
		this.setID(id);	
	}
	
	public String getID(){ return id;}
	
	public void setID(String id) { this.id=id; }

}
