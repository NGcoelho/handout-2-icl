package environment;
import ast.ASTNode;

public class Binder{

	private String id;
	private ASTNode expression;

	public Binder(String id, ASTNode expr){
		this.id=id;
		expression=expr;
	}
	
	public String getID(){
		return id;
	}
	
	public ASTNode getExpression(){
		return expression;
	}
	
	@Override
	public boolean equals(Object obj){
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		Binder other = (Binder)obj;
		if(expression == null){
			if(other.expression != null) return false;		
		}
		if(id == null){
			if(other.id != null) return false;
		}
		else if(!id.equals(other.id)) return false;
		return true;	
	}
	
}
