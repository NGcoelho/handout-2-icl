package environment;
import compUtils.VarLoc;
import java.util.ArrayList;


public class CompileEnviroment<T> extends  Environment<T> {
    private int frameCount;
    public static class Frame{
        private String id;
        private int offset;

        public Frame(String id, int offset){
            this.id = id;
            this.offset=offset;
        }
    };

    private ArrayList<Frame> frameList;
    private CompileEnviroment<T> up;

    public CompileEnviroment(){
        super();
        this.frameList = new ArrayList<>();
    }

    private CompileEnviroment(CompileEnviroment<T> compileEnviroment){
        this();
        up = compileEnviroment;
    }

    public CompileEnviroment<T> beginScope(){
        return new CompileEnviroment<T>(this);
    }

    public CompileEnviroment<T> endScope(){
        return up;
    }
    //NOVO ENVIRONMENT COM O MESMO PAI QUE ESTE
    public CompileEnviroment<T> dupEnvironment(){
        return new CompileEnviroment<T>(this.up);
    }

    public int addFrame(String id){
        frameList.add(new Frame(id,frameCount));
        frameCount++;
        return frameCount-1;
    }

    public VarLoc doLookup(String id){
        //PROCURAMOS A PARTIR DA FRAME LOCAL PARA BAIXO
        CompileEnviroment<T> curr = this;
        int n_frames = 0;
        while(curr != null){
            for(Frame frame : curr.frameList){
                if(frame.id.equals(id))
                    return new VarLoc(n_frames,frame.offset);
            }
            curr = curr.up;
            n_frames++;
        }
        //mb lançar exception?
        return null;
    }



}
