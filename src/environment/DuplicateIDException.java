package environment;

public class DuplicateIDException extends Exception {
	
	private static final long serialVersionUID = 1L;	
	
	private String id;

	public DuplicateIDException(String id) {
		this.setID(id);	
	}
	
	public String getID(){ return id;}
	
	public void setID(String id) { this.id=id; }

}
