package environment;

import values.IValue;
import values.ObjectValue;

import java.util.*;

public class Environment<T> {

	public static class Assoc<T>{
		String id;
		T value;
		Assoc(String id, T value){
			this.id=id;
			this.value=value;		
		}	
	}
	
	Environment<T> up;
	ArrayList<Assoc<T>> assocs;

	public Environment() {
		this.up=null;
		this.assocs=new ArrayList<Assoc<T>>();
	}
	
	private Environment(Environment<T> up){
		this();
		this.up=up;
	}

	public T find(String id) throws UndeclaredIDException {
		Environment<T> current=this;
		while(current != null) {
			for (Assoc<T> assocs : current.assocs){
				if (assocs.id.equals(id)) return assocs.value;
				else if( assocs.value instanceof ObjectValue){
					try{
						return (T) ((ObjectValue)assocs.value).getFromLabel(id);
					}
					catch(UndeclaredIDException e){
					 //do nothing and let it seek other environments
					}
				}
			}
			current = current.up;
		}

		throw new UndeclaredIDException(id);
	}

	public Environment<T> beginScope(){
		return new Environment<T>(this);
	}

	public Environment<T> endScope(){
		return up;
	}

	public void associate(String id, T value) throws DuplicateIDException {
		for(Assoc<T> assoc : assocs)
			if(assoc.id.equals(id))
				throw new DuplicateIDException(id);
		assocs.add(new Assoc<T>(id,value));



	}
}
