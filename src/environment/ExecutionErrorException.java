package environment;

public class ExecutionErrorException extends  Exception {

    private static final long serialVersionUID = 1L;

    public  ExecutionErrorException(String message){
        super(message);
    }
}
