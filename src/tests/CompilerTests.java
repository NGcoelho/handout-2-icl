

import console.CompilerShell;
import environment.BadTypeException;
import environment.DuplicateIDException;
import environment.UndeclaredIDException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CompilerTests {

    private static String BASH = "/bin/bash";

    private void testCase(String expression, String result) throws IOException, InterruptedException, UndeclaredIDException, BadTypeException, ParseException, DuplicateIDException {
        Process p;
        System.out.println("------------------NEW EXPRESSION--------------------------");
        System.out.println("Removing previous jasmin files and java classes\n");
        p = Runtime.getRuntime().exec(new String[]{BASH, "-c","rm *.j, *.class"});
        p.waitFor();
        System.out.println("Compiling expression to jasmin source code\n");
        CompilerShell.compile(expression);
        System.out.println("Compiling expression to jasmin bytecode(.class)\n");
        p = Runtime.getRuntime().exec(new String[]{BASH,"-c", "java -jar jasmin.jar *.j"});
        p.waitFor();
        assertTrue(p.exitValue() == 0, "Bytecode Compiled");
        System.out.println(readOutput(p).toString());
        p = Runtime.getRuntime().exec(new String[]{BASH,"-c", "java Demo"});
        p.waitFor();
        String final_result = readOutput(p);
        System.out.println("Result Output: "+ final_result+"\n");
        System.out.println("-----------------------------------------------------------");
        assertTrue(result.equals(final_result));
    }

    private String readOutput(Process p) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuffer output = new StringBuffer();
        String line = "";
        while((line = reader.readLine()) != null){
            output.append(line+"\n");
        }
        return output.toString();
    }

    @Test
    public void TestCase1() throws Exception{
        testCase("1+1\n","2\n");
        testCase("2*2+(1+4)\n","9\n");
        testCase("2*5*5+2\n","52\n");
        testCase("8/4/2\n","1\n");
    }

    @Test
    public void TestCase2() throws Exception{
        testCase("true\n","1\n");
        testCase("true && false\n","0\n");
        testCase("true || false\n","1\n");
        testCase("if( (true && false) == false ) then true else false end\n","1\n");
    }

    @Test
    public void TestCase3() throws Exception{
        testCase("let x:int = 2 in x + 2 end\n","4\n");
        testCase("let x:int = 1 y:int = 8 in x + y end\n","9\n");
        testCase("let x:int = let y:int = 3 in 10*y end in x / 10 end\n","3\n");
        testCase("let x:int = 1 y:int = 2 in let x: int = 3 z:int = 4 in x+y+z end end\n","9\n");
    }

    @Test
    public void TestCase4() throws Exception{
        testCase("let x:int = var(1) in x := 2; *x + 1 end\n","3\n");
        testCase("let x:int = var(1) in *x end\n","1\n");
        testCase("let x:int = var(1)  in while *x < 10 do x := *x + 1 end ; *x - 5 end\n","5\n");
        testCase("let x:int = var(1)  in if *x == 1 then while *x < 10 do x := *x + 1 end else x:=10 end; *x end\n","10\n");
    }

    @Test
    public void TestCase5()throws Exception {
        testCase("let f:int = fun n:int => n+1 end in f(2) end\n","3\n");
        testCase("let f:int = fun n:int,g:(int)->int => g(n) end in f(2,fun x:int=>x+1 end) end\n","3\n");
        testCase("fun x:int=> fun y:int=>x+y+1 end end(3)(2)\n","6\n");
        testCase("let f:int = fun n:int,g:(int)->int => g(n) end y:int = fun n:int=>n+1 end in f(3,y) end\n","4\n");
    }

    @Test
    public void TestCase6() throws  Exception{
        testCase("letrec x:int = 3 y:int=x in y end\n","3\n");
        testCase("letrec fact:(int)->int = fun n:int=> if n < 1 then 1 else n*fact(n-1) end end in fact(5) end\n","120\n");
        testCase("letrec fib:{{a:(int)->int}} = {{a = fun x:int => if x == 0 then 0 else if x == 1 then 1 else fib.a(x-1)+fib.a(x-2) end end end}} in fib.a(20) end\n","6765\n");
    }

    @Test
    public void TestCase7() throws  Exception{
        testCase("let a:int  = {a = 3} in a.a end\n","3\n");
        testCase("let a:int = {b = fun x:int => x+1 end} in a.b(3) end\n","4\n");
        testCase("let a:int = 1 in let b:int ={c = fun x:int => x + a end} in b.c(3) end end\n","4\n");
    }

    @Test
    public void TestCase8() throws Exception{
        testCase("let counter:int = {{n =var(0), increment = fun => this.n:= *this.n+1 end}} in counter.increment();*counter.n end\n","1\n");
        testCase("let cores:int = {verde = 1, amarelo = 2, vermelho = 0} in " +
                "letrec semaforo:{{cor:ref(int), avancar:()->int, verde:()->bool," +
                " amarelo:()->bool, vermelho:()->bool}} = {{cor = var(0), avancar = fun => if *this.cor == cores.vermelho then this.cor := cores.verde" +
                " else if *this.cor == cores.verde then this.cor := cores.amarelo else this.cor := cores.vermelho end end end, verde = fun => *this.cor == cores.verde end," +
                " amarelo = fun => *this.cor == cores.amarelo end,vermelho = fun => *this.cor == cores.vermelho end }} " +
                "in while !semaforo.amarelo() do semaforo.avancar() end;" +
                " semaforo.amarelo() end end\n", "1\n");
        testCase("let " +
                "  g:bool = fun f:()->bool => f() end " +
                "  x:int = var(10) " +
                "in " +
                "  g(" +
                "    fun => " +
                "      while *x > 0 do " +
                "        x := *x - 1 " +
                "      end ; true " +
                "    end " +
                "  ) " +
                "end\n" , "1\n");
        testCase("letrec" +
                "  counter:{{count:ref(int), increment:()->int}} = {{" +
                "    count = var(1)," +
                "    increment = fun => this.count := *this.count + 1 end" +
                "  }}" +
                "in" +
                "  fun x:{{count:ref(int), increment:()->int}} => " +
                "    while *x.count < 6 do" +
                "      x.increment()" +
                "    end; *x.count " +
                "  end(counter)" +
                "end\n", "6\n");

    }

}
