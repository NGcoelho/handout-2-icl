package types;

public interface IType {

    public IType typecheck();

    public IType getInstance();

    public String getLabel();
}
