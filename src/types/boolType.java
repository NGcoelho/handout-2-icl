package types;

import values.ObjectValue;

public class boolType implements  IType{
    public static boolType instance = new boolType();

    @Override
    public IType typecheck() {
        return instance;
    }

    @Override
    public IType getInstance() {
        return instance;
    }

    @Override
    public String getLabel() {
        return "I";
    }

    @Override
    public String toString(){
        return "bool";
    }

    @Override
    public  boolean equals(Object other){
        if (other instanceof boolType)
            return getInstance() == ((boolType)other).getInstance();
        return false;
    }

    @Override
    public int hashCode(){return 37;}


}


