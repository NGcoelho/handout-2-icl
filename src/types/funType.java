package types;

import java.util.List;

public class funType implements IType {

    public static funType instance = new funType();

    private List<IType> types;
    private IType returnType;
    private String label;

    public funType(){

    }

    public funType(List<IType> parameterTypes, IType returnType){
        types = parameterTypes;
        this.returnType = returnType;
        label = "fun_";

        for (IType type : types){
            if(type instanceof  funType)
                label+="L"+type.getLabel()+";";
        }
        label += ";" + returnType.getLabel();
    }

    public IType getReturnType(){
        return returnType;
    }

    public List<IType> getParamTypes(){
        return types;
    }

    @Override
    public IType typecheck() {
        return this;
    }

    @Override
    public IType getInstance() {
        return instance;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label){ this.label = label;}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result +((returnType == null) ? 0 : returnType.hashCode());

        for(IType type : types){
            result = prime * result + ((type == null) ? 0 : type.hashCode());
        }

        return result;
    }

    @Override
    public boolean equals (Object obj){
        if(this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        funType other = (funType) obj;
        if(returnType == null ){
            if(other.returnType != null)
                return false;

        }
        else if(!returnType.equals(other.returnType))
            return false;
        if(types == null){
            if(other.types != null)
                return false;
        }
        else {
            if(types.size() != other.types.size())
                return false;
            else{
                for (int i = 0; i< types.size(); i++){
                    IType a = types.get(i);
                    IType b = other.types.get(i);
                    if(!a.equals(b)) return false;
                }
            }
        }
        return true;
    }


    @Override
    public String toString(){
        String type = "fun(";
        for(IType t : types) {
            type += t.toString();
        }
        type += ") => "+ returnType.toString();
        return type;
    }
}
