package types;

public class intType implements IType{
    public  static intType instance = new intType();

    @Override
    public IType typecheck() {
        return instance;
    }

    @Override
    public IType getInstance() {
        return instance;
    }

    @Override
    public String getLabel() {
        return "I";
    }

    @Override
    public String toString(){
        return "int";
    }

    @Override
    public boolean equals(Object other){
        if(other instanceof  intType)
            return getInstance() == ((intType)other).getInstance();
        return false;
    }
    @Override
    public int hashCode(){
        return 53;
    }
}
