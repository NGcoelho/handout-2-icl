package types;

import environment.UndeclaredIDException;

import java.util.LinkedHashMap;
import java.util.Map;

public class objType implements IType {

    public static IType instance = new objType();

    private Map<String,IType> types;



    public objType(){
        types = new LinkedHashMap<>();
    }

    public objType(Map<String,IType> types){
        this.types=types;
    }

    @Override
    public IType typecheck() {
        return this;
    }

    @Override
    public IType getInstance() {
        return instance;
    }

    @Override
    public String getLabel() {
        return "";
    }

    public  void addType(String id, IType t){
        this.types.put(id,t);
    }

    public IType getType(String id) throws UndeclaredIDException {
        if(!types.containsKey(id)){
            throw new UndeclaredIDException("Id "+ id +" not found");
        }
        return types.get(id);
    }

    public int getOffset(String id) throws UndeclaredIDException {
        int i=0;
        for(Map.Entry<String,IType> e :types.entrySet()){
            if(e.getKey().equals(id))
                return i;
            i++;
        }
        throw new UndeclaredIDException("Identifier not found");
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        objType other = (objType) obj;
        if(types == null){
            if(other.types != null)
                return false;
        } else if (!types.equals(other.types)) {
            for(Map.Entry<String,IType> e : other.types.entrySet()){
                if(!types.containsKey(e.getKey())){
                    return false;
                }
                IType t = types.get(e.getKey());
                if(!t.equals(e.getValue()))
                    return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        for(Map.Entry<String,IType> e: types.entrySet()){
            int k_hash = e.getKey().hashCode();
            int t_hash=e.getValue().hashCode();
            result = prime * result + k_hash + t_hash;
        }
        return result;
    }
}
