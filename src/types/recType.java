package types;

import environment.Binder;
import environment.UndeclaredIDException;

import java.util.List;

public class recType implements IType {

    public static IType instance = new recType();

    public List<Binder> bindings;

    public  String label = "";

    private recType(){

    }

    public recType(List<Binder>b){
        bindings = b;
    }


    @Override
    public IType typecheck() {
        return this;
    }

    @Override
    public IType getInstance() {
        return instance;
    }

    @Override
    public String getLabel() {
        return label;
    }

    public void setLabel(String l){
        label = l;
    }
    public Binder getBinding(String id) throws UndeclaredIDException {
        for(Binder b : bindings){
            if(b.getID().equals(id))
                return b;
        }
        throw new UndeclaredIDException("Id : "+id+" not found");
    }

    public int getOffset(String id) throws UndeclaredIDException {
        for(int i = 0; i < bindings.size(); i++){
            Binder b = bindings.get(i);
            if(b.getID().equals(id))
                return i;
        }
        throw new UndeclaredIDException("Id : "+id+" not found");

    }

    public IType getBindingType(String id) throws UndeclaredIDException {
        return getBinding(id).getExpression().getType();
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        for (Binder b: bindings)
            result = prime*result * prime*(b.getExpression().getType() == null ? 0 : b.getExpression().getType().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        recType other = (recType) obj;
        if(bindings == null){
            if(other.bindings != null) return false;
        }
        else if(bindings.size() != other.bindings.size())
            return false;
        else
            for(Binder b : other.bindings){
                if(!bindings.contains(b)) return false;
            }
        return true;
    }

}
