package types;

public class refType implements  IType{

    public static refType instance = new refType();
    public IType type;

    private refType(){}

    public refType(IType type){
        this.type=type;
    }

    @Override
    public IType typecheck() {
        return this;
    }

    @Override
    public IType getInstance() {
        return instance;
    }

    @Override
    public String getLabel() {
        return "ref_"+type.getLabel();
    }

    @Override
    public  String toString(){
        return "ref("+type.toString()+")";
    }
    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime*result + ((type == null ) ? 0 : type.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object other){
        if(other instanceof refType){
            refType obj = (refType)other;
            if(type == null){
                if(obj.type != null)
                    return false;
            }
            else if(!type.equals(obj.type))
                return false;
            return true;
        }
        else return false;
    }
}
