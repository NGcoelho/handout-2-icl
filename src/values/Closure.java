package values;

import ast.ASTNode;
import environment.Environment;
import types.IType;

import java.util.List;
import java.util.Map;


public class Closure implements IValue {
	private List<String> parameters;
	private ASTNode body;
	private Environment<IValue> environment;

	public Closure(List<String> parameters, ASTNode body, Environment<IValue> env){
		this.parameters=parameters;
		this.body=body;
		this.environment=env;
	}
	public List<String> getParameters(){
		return parameters;
	}
	public ASTNode getBody(){
		return body;
	}
	public Environment<IValue> getEnv(){
		return environment;
	}
}
