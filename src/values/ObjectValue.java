package values;

import environment.Environment;
import environment.UndeclaredIDException;

import java.util.Map;

public class ObjectValue implements IValue {
	private Map<String, IValue> values;
	private Environment<IValue> env;
	public ObjectValue(Map<String, IValue> values, Environment<IValue> env){
		this.values=values;
		this.env=env;	
	}
	public Environment<IValue> getEnv(){
		return env;	
	}
	public IValue getFromLabel(String id) throws UndeclaredIDException {
		IValue v = values.get(id);
		if (id.equals("this")) return this;
		if(v==null)
			throw new UndeclaredIDException("Missing ID");

		return v;	
	}
	@Override
	public String toString(){
		return "{{"+values.toString()+"}}";	
	}
}
