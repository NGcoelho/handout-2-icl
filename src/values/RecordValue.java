package values;

import environment.UndeclaredIDException;

import java.util.Map;

public class RecordValue implements IValue {
	private Map<String, IValue> values;
	public RecordValue(Map<String, IValue> values){
		this.values=values;	
	}
	public IValue getFromLabel(String id) throws UndeclaredIDException {
		IValue v = values.get(id);
		if(v==null)
			throw new UndeclaredIDException("Missing ID");
		return v;	
	}
	@Override
	public String toString(){
		return "{"+values.toString()+"}";	
	}
}
