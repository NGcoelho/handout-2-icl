package values;

public class ReferenceValue implements IValue {
	private IValue value;

	public ReferenceValue(IValue value){ this.value=value; }

	public IValue getValue(){ return value; }

	public void setValue(IValue value) { this.value=value; }
}
